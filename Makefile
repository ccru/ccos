SOURCEDIR=src

SRC=$(shell find $(SOURCEDIR) -name "*.c" -o -name "*.asm")
REAL=$(shell find $(SOURCEDIR) -name "*.real")
INCLUDE:=include

CC=x86_64-elf-gcc
CFLAGS= -g3                            \
		-Og                            \
		-ffreestanding            	   \
		-I$(INCLUDE)				   \
		-fno-pic                       \
		-mcmodel=kernel				   \
		-mno-sse                       \
		-mno-sse2                      \
		-mno-mmx                       \
		-mno-80387                     \
		-mno-red-zone                  \
		-m64                           \
		-march=x86-64                  \
		-fno-stack-protector           \
		-fno-omit-frame-pointer
OBJS=$(patsubst %.c, %.o, $(patsubst %.asm, %.o, $(SRC)))
BINS=$(patsubst %.real, %.bin, $(REAL))

IMG=ccos.img

EMU=qemu-system-x86_64
EFLAGS=-boot c -s -S -d int -m 1024M -no-reboot -debugcon file:dbg -monitor stdio -no-shutdown -machine q35 -hda $(IMG)

LDFLAGS=-T misc/linker.ld       \
        -o ccos.elf           \
        -nostdlib               \
		-no-pie                 \
	    -z max-page-size=0x1000

all: $(BINS) $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS)

%.o: %.asm
	nasm  $^ -felf64 -gdwarf -o $@

%.o: %.c
	$(CC) -c $^ -o $@ $(CFLAGS)

%.bin: %.real
	nasm $^ -fbin -o $@

iso: all
	rm -f $(IMG)
	dd if=/dev/zero bs=1M count=8 seek=64 of=$(IMG)
	parted -s $(IMG) mklabel msdos
	parted -s $(IMG) mkpart primary 1 100%
	echfs-utils -m -p0 $(IMG) quick-format 512
	echfs-utils -m -p0 $(IMG) import misc/qloader2.cfg qloader2.cfg
	echfs-utils -m -p0 $(IMG) import ccos.elf ccos.elf
	misc/qloader2-install misc/qloader2.bin $(IMG)

run: iso
	$(EMU) $(EFLAGS)

.PHONY : clean iso run syms
clean:
	rm -f $(OBJS)
	rm -f ccos.elf
	rm -f $(BINS)
	rm -f $(IMG)
